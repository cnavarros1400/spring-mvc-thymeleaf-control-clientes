package com.luden.dao;

import com.luden.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Ing. Svein Navarro
 */
public interface UsuarioDao extends JpaRepository<Usuario, Long>{
    Usuario findByUserName(String userName);//nombre del metodo usado por spring
}
