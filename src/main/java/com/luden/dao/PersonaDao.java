/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.luden.dao;

import com.luden.domain.Persona;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Ing. Svein Navarro
 */

public interface PersonaDao extends CrudRepository<Persona, Integer>{
    
}
