package com.luden.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import lombok.Data;


/**
 *
 * @author Ing. Svein Navarro
 */
@Data
@Entity
@Table(name = "persona")
public class Persona implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idPersona")
    private Integer idPersona;
    
    @NotEmpty
    private String nombre;
    
    @NotEmpty
    private String apellidos;
    
    private String telefono;
    
    @NotEmpty
    @Email(message = "Favor de capturar formato de correo valido")   
    private String email;
}
