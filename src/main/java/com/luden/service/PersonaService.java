package com.luden.service;

import com.luden.domain.Persona;
import java.util.List;

/**
 *
 * @author Ing. Svein Navarro
 */
public interface PersonaService {
    public List<Persona> listarPersonas();
    public void guardar (Persona persona);
    public void eliminar (Persona persona);
    public Persona encontrarPersona (Persona persona);
}
