package com.luden.web;

import com.luden.domain.Persona;
import com.luden.service.PersonaService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Ing. Svein Navarro
 */
@Controller
@Slf4j
public class ControladorInicio {
    
    @Autowired        
    private PersonaService personaService;
    
    @GetMapping("/")
    public String inicio(Model model, @AuthenticationPrincipal User usuario){
        Iterable<Persona> personas = personaService.listarPersonas();
        log.info("Ejecutando controlador Spring MVC");
        log.info("Usuario con acceso en sistema: " + usuario);
        model.addAttribute("personas", personas);
        return "index";

    }
    
    @GetMapping("/agregar")
    public String agregar(Persona persona){
        return "modificar";
    }
    
    @PostMapping("/guardar")
    public String guardar(@Valid Persona persona, BindingResult errores){
        if (errores.hasErrors()) {
            return "modificar";
        }
        personaService.guardar(persona);
        return "redirect:/";
    }
    //usando path variable
    @GetMapping("/editar/{idPersona}")
    public String editar(Persona persona, Model model){
        persona = personaService.encontrarPersona(persona);
        model.addAttribute("persona", persona);
        return "modificar";
    }
//    usando path variable    
//    @GetMapping("/eliminar/{idPersona}")
//    public String eliminar(Persona persona, Model model){
//        personaService.eliminar(persona);
//        return "redirect:/";
//    }
    
    //usando query param
    @GetMapping("/eliminar")
    public String eliminar(Persona persona){
        personaService.eliminar(persona);
        return "redirect:/";
    }
    
}
