package com.luden.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Ing. Svein Navarro
 */
public class EcriptarPassword {
    
    public static void main(String[] args) {
        
        String password = "123";
        System.out.println("PASSWORD ORIGINAL: " + password);
        System.out.println("**ENCRIPTANDO PASSWORD**");
        System.out.println("PASSWORD ENCRIPTADO: " + encriptarPassword(password));
        
    }
    
    public static String encriptarPassword(String pass){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(pass);
    }
    
}
